package apitesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class RestAssuredAssignment1 {
  @Test
  public void firstGetSingleUser() {
	  baseURI = "https://reqres.in/";
		given().get("/api/users/2").then().statusCode(200);
  }
  @Test
	public void testparticularvalue() {
		baseURI = "https://reqres.in/";
		given().get("/api/users/2").then().body("data.last_name", equalTo("Weaver"));
}
}