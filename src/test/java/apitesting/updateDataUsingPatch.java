package apitesting;

import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class updateDataUsingPatch {
  @Test
  public void PatchData() {
	  JSONObject request=new JSONObject();
		 request.put("name", "karthick");
		 request.put("job", "qa1");
		 given().body(request.toJSONString()).patch("https://reqres.in/api/users/2").then().statusCode(200).body("updatedAt", greaterThanOrEqualTo("2023-01-24T10:12:04.116Z"));
  }
}
