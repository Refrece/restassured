package apitesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;
public class RestAssuredAssignment2 {
  @Test
  public void AddBearerToken() {
	  //d19c4f66a3327e8e8b9af532ae8684bf4d9012502970b3bbbd888ac93d5a2b30
		 JSONObject request=new JSONObject();
		 request.put("name", "karthi");
		 request.put("email", "tkarthi@gmail.com");
		 System.out.println(request);
		 String token="d19c4f66a3327e8e8b9af532ae8684bf4d9012502970b3bbbd888ac93d5a2b30";
		 given().header("Authorization","Bearer"+token).body(request.toJSONString()).when().post("https://gorest.co.in/public/v2/users").then().statusCode(201);
  }
}
