package apitesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class TestingGetResponse {
  @Test
  public void TestingValue() {
	  baseURI = "https://reqres.in/";
		given().get("/api/users?page=2").then().body("data.first_name", hasItems("Byron", "George"))
				.body("data.last_name", hasItems("Fields", "Edwards"));
	}
  
  }

