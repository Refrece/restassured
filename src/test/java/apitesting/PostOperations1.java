package apitesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;
public class PostOperations1 {
  @Test
  public void postOperationTest() {
	 JSONObject request=new JSONObject();
	 request.put("name", "karthi");
	 request.put("job", "qa");
	 System.out.println(request);
	 baseURI="https://reqres.in/api";
	 given().body(request.toJSONString()).when().post("/users").then().statusCode(201);
  }
}
