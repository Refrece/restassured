package apitesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class TestingGetUsingGivenWhenApproach {
  @Test
  public void testStatuscode() {
	  baseURI = "https://reqres.in/";
		given().get("/api/users?page=2").then().statusCode(200);
	}
	@Test
	public void testparticularvalue() {
		baseURI = "https://reqres.in/";
		given().get("/api/users?page=2").then().body("data[1].email", equalTo("lindsay.ferguson@reqres.in"));
	}
	// to fetch all details
	@Test
	public void printallvalue() {
		baseURI = "https://reqres.in/";
		given().get("/api/users?page=2").then().log().all();
	}
  }

