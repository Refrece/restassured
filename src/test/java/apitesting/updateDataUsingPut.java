package apitesting;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class updateDataUsingPut {
  @Test
  public void UpdateDaa() {
	  JSONObject request=new JSONObject();
		 request.put("name", "karthi");
		 request.put("job", "qa");
		 given().body(request.toJSONString()).put("https://reqres.in/api/users/2").then().statusCode(200).body("updatedAt", greaterThanOrEqualTo("2023-01-24T10:08:04.150Z"));
  }
}
