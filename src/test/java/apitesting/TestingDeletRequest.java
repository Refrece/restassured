package apitesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class TestingDeletRequest {
  @Test
  public void DeleteOperation() {
	 baseURI="https://reqres.in/" ;
	 given().when().delete("api/users/2").then().statusCode(204);
  }
}
